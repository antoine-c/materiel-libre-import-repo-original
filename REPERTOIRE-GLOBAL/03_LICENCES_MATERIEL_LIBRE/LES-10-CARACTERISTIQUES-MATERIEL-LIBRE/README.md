## EXPLICATIONS À PROPOS DU TEXTE "LES 10 CARACTÉRISTIQUES D'UN MATÉRIEL LIBRE"

_Fichier README.md_

Dans le répertoire où se situe ce fichier README.md, se trouve aussi un texte portant le titre "les 10 caractéristiques d'un matériel libre" et qui est mis à disposition sous plusieurs formats numériques: 
*  Hypertext Markup Language (.html), 
*  Open Document Format (.odt), 
*  Portable Document Format (.pdf), 
*  Markdown (.md), 
*  Texte brut (.txt).

Le format de fichier est indiqué au début du nom de chaque fichier - le format est aussi identifiable par l'extension de chaque fichier.


**Ce texte "les 10 caractéristiques d'un matériel libre", a pour but de décrire ce que pourraient être les 10 caractéristiques d'un matériel libre, au sens absolu du terme.**

L'espoir de ce texte "les 10 caractéristiques d'un matériel libre", est de créer un "étalon" pour la notion de Matériel Libre. 

Si ce texte est acceptable et accepté, il sera alors possible:

1. de situer un matériel par rapport à cet étalon de Matériel Libre, pris au sens absolu et canonique du terme; 
2. de rédiger des lots de licences spécifiques aux fabrications de choses matérielles, en indiquant ce que proposent chacune de ces licences par rapport à cet étalon de Matériel Libre pris au sens absolu et canonique du terme ( et peut-être qu'un jour, il existera un jeu de licences un peu comme les Creative Commons, mais spécifiques pour les fabrications ...).

Pour que ce texte "les 10 caractéristiques d'un matériel libre" soit recevable, il sera nécessaire qu'il soit audité et accepté pour ce qu'il est, pour sa fonction, tout à la fois et au moins par:

*  les plus hautes sommités internationales dans le domaine des licences libres - notamment les personnes impliquées dans la rédaction des licences libres dans une optique d'utilisation de ces licences pour la mise à disposition d'œuvres matérielles.
*  la science et la recherche dans le domaine de l'utilisation des licences libres pour la mise à disposition d'œuvres matérielles
*  la plus large partie possible de personnes praticiennes, utilisant, ou souhaitant utiliser, les licences libres pour la mise à disposition de leurs œuvres matérielles (choses faites avec des atomes).

Pour arriver à obtenir cette acceptation internationale, il paraît nécessaire, à minima, de traduire ce texte en langue anglaise, dans un niveau dit "anglais international" - c'est à dire, sans recourir aux nuances de langage. Si vous souhaitez participer à cette traduction, merci de nous faire signe au travers de cette instance framagit: prenez un compte framagit, et demandez à faire partie du projet.

Le texte "les 10 caractéristiques d'un matériel libre" est mis à disposition sous droits restreints. Ces droits restreints, sont stipulés au début et à la fin du texte "les 10 caractéristiques d'un matériel libre". Une copie de ces droits restreints, est jointe avec le texte "les 10 caractéristiques d'un matériel libre". On trouve cette copie dans le présent répertoire où se trouvent ce fichier README.md et les fichiers du texte "les 10 caractéristiques d'un matériel libre".


---
