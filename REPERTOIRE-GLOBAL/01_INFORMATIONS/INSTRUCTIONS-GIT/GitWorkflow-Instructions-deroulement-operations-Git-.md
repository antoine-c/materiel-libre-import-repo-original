---
author: antoine c
description: |
    Projet de rédaction d'instructions git - workflow simple, mono projet ou
    multi projet
title: Instructions Git Mono Projet et Multi Projets
---



## Description simplifiée

Convention de nomination des branches: 

  * **CATÉGORIE_DE_LA_BRANCHE** en majuscules, suivi d'un "**/**", suivi de **nom_de_la_branche** en minuscules

Catégories de branches:

  * **PROPOSITIONS** # branche éphémère pour proposer des améliorations
  * **VERSIONNAGE** # branche éphémère pour préparer une version
  * **CORRECTIF** # branche éphémère pour effectuer une correction sur une version

Catégorie supplémentaire dans le cas de "1 dépôt= x projets":

  * **PROJET** # branche éternelle pour versionner les projets

Exemple de nom de branches:

  * PROPOSITION/ma_jolie_proposition
  * VERSIONNAGE/3.0.b.2
  * CORRECTIF/3.0.b.3
  * PROJET/10_caracteristiques

### Proposer une amélioration

Logique de workflow retenue: workflow simple.

Une proposition est branchée sur master, travaillée, puis fusionnée dans master.

```
| master
O
|\
| | PROPOSITIONS/maJolieProposition
|/
0
| master
```

#### Phase 1: être à jour

```
# Se placer sur la branche master
$ git checkout master

# Se mettre à jour du dépôt distant
$ git pull origin master

# Vérifier que tout va bien
$ git status
```

#### Phase 2: créer la branche de proposition

```
# Se placer sur la branche master
git checkout master

# Consulter la liste des branches
$ git branch -a

# Créer une branche de proposition en local
$ git branch PROPOSITIONS/ma_proposition

# Se placer sur la branche de proposition
$ git checkout PROPOSITIONS/ma_proposition

# Créer la branche de proposition sur le dépôt distant
$ git push -u origin PROPOSITIONS/ma_proposition
```

#### Phase 3: travailler sur la branche

À répéter pour chaque lots de travaux effectués à enregistrer dans git (ajouts,
retraits, suppressions, de fichiers ou de dossiers).

##### a) avant de travailler sur la proposition

```
# Se positionner sur la branche de proposition
$ git checkout PROPOSITIONS/ma_proposition

# Importer les informations du dépôt distant
$ git fetch origin

# Rembobiner l'historique de la branche master
$ git rebase origin/master

# Rembobiner l'historique de la branche de proposition
$ git rebase origin/PROPOSITIONS/ma_proposition
```


##### b) effectuer localement les changements dans le répertoire git dédié

Travailler dans le répertoire: modifications, ajouts, supression, de fichiers ou de répertoires.


##### c) enregistrer/publier les travaux effectués

```
# Se positionner dans la branche de la proposition
$ git checkout PROPOSITIONS/ma_proposition

# Enregistrer les changements en local
$ git add -A .

# Publier les changements en local
$ git commit
# commenter dans l'éditeur: 1 ligne = Titre; puis retour chariot 2 fois puis lignes de détails

# Pousser les changements vers le dépôt distant
$ git push -u origin PROPOSITIONS/ma_proposition
```


#### Phase 4: fusionner une fois le travail abouti

Seulement une fois que les travaux sont aboutis, et pas avant SINON
DANGER!

```
# Se placer sur la branche de la proposition en local
$ git checkout PROPOSITIONS/ma_proposition

# Faire une demande de fusion vers le dépôt distant
$ git push -o merge_request.create -o merge_request.target=PROPOSITIONS/ma_proposition

# Se placer sur la branche master en local
$ git checkout master

# Obtenir les éventuels changements effectués entre temps sur la branche master du dépôt distant
$ git pull origin master

# Fusionner la branche de la proposition dans la branche master, en local
$ git merge --no-ff PROPOSITIONS/ma_proposition

# Pousser la fusion vers le dépôt distant
$ git push origin master

# Détruire la branche de proposition en local
$ git branch -d PROPOSITIONS/ma_proposition

# Détruire la branche de proposition sur le dépôt distant
$ git push origin --delete PROPOSITIONS/ma_proposition
```

### Versionner

*Deux situations sont envisageables:*

  * 1ère situation: **1 dépôt = 1 projet** => un numéro de version est donné sur master
  * 2ème situation: **1 dépôt = plusieurs projets indépendants** => un numéro de version est donné sur la branche de chaque projet versionnable

*Résumé schématique:*


```
CAS N°1:
--------
(début)
|
| master 
|\
| | VERSIONNAGE/0.1.a.1
|/
| master tag [0.1.a.1]
|
(fin)
```


```
CAS N°2
-------
(début)
|
| master
|
|\_________________________
|                          \ projet/Nom_Projet
|\                         |
| | VERSIONNAGE/A-0.1.a.1  |
| |__________________      |
|                    \_____|
|                          | projet/Nom_Projet tag [Nom_Projet-0.1.a.1]
|                          |
| master                   |
|                          |
(fin)                     (fin)

```


#### Cas n°1: versionnage quand 1 dépôt = 1 projet

Dans cette situation, une branche éphémère de versionnage est créée sur master, puis vignettée avec le numéro de version, pour ensuite être fusionnée avec master qui devient estampillée du numéro de version.

```
(début)
|
| master 
|\
| | VERSIONNAGE/0.1.a.1
|/
| master tag [0.1.a.1]
|
(fin)
```

##### a) Commencer par choisir le numéro de version

Exemples de convention de numéros de version ("a" pour alpha; "b" pour beta; "rc" pour release candidate; rien pour version stable)
  * 0.1
  * 0.1.a.1
  * 3.2.rc.7
  * 4.1.0

```
# Se placer sur la branche master en local
$ git checkout master

# Repérer les versions déjà  existantes
$ git tag

# Puis décider du numéro de version (le noter pour la suite)
```
##### b) Repérer le numéro de commit abbrégé, sur lequel doit être basée la version

Une version est basée sur un numéro de commit. C'est la règle. Il est donc nécessaire de repérer le numéro de commit sur lequel se base la version que l'on veut créer. Plutôt que de se référer au long numéro de commit, il est plus facile de se référer à son numéro abbrégé, repérable avec la commande `git log --abbrev-commit`

```
# Se placer sur la branche master
$ git checkout master

# Lister les numéros de commits abbrégés
$ git log --abbrev-commit

# Repérer le numéro de commit sur lequel doit être fondé la version (le noter pour la suite)
```

##### c) Réaliser le versionnage proprement dit

Dans les commandes qui suivent:

  * NumVersion = le numéro de version,
  * NumComit = le numéro de commit

Remplacer ces termes par les numéros de version et de commit.

```
# Se placer sur la branche master en local
$ git checkout master

# Créer et se placer sur une branche éphémère de versionnage en local
$ git checkout -b VERSIONNAGE/NumVersion NumCommit

# Générer une étiquette contenant la branche de versionnage en local, portant le numéro de version souhaité
$ git tag NumVersion

# Revenir sur la branche master en local
$ git checkout master

# Fusionner la branche de versionnage, dans la branche master en local
$ git merge VERSIONNAGE/NumVersion

# Pousser la version effectuée localement contenu dans le tag, vers la branche du projet spécifique sur le dépôt distant, via l'étiquettage de version contenant la version
$ git push --tags origin master

# Supprimer la branche éphémère de versionnage en local
$ git branch -d VERSIONNAGE/NumVersion

# Supprimer la branche éphémère de versionnage sur le dépôt distant
$ git push origin --delete VERSIONNAGE/NumVersion
```

#### Cas n°2 : 1 dépôt = plusieurs projets indépendants


```
CAS N°2
-------
(début)
|
| master
|
|\_________________________
|                          \ projet/Nom_Projet
|\                         |
| | VERSIONNAGE/A-0.1.a.1  |
| |__________________      |
|                    \_____|
|                          | projet/Nom_Projet tag [Nom_Projet-0.1.a.1]
|                          |
| master                   |
|                          |
(fin)                     (fin)
```

##### a) Commencer par choisir le numéro de version

Exemples de convention de numéros de version pour des branches `projet/Nom_Projet` ("a" pour alpha; "b" pour beta; "rc" pour release candidate; rien pour version stable)
  * [Nom_Projet]-0.1
  * [Nom_Projet]-0.1.a.1
  * [Nom_Projet]-3.2.rc.7
  * [Nom_Projet]-4.1.0

```
# Se placer sur la branche master en local
$ git checkout master

# Repérer les numéros de versions déjà existants
$ git tag



# Puis décider du numéro de version à donner (le noter pour la suite)
```

##### b) Repérer le numéro de commit abbrégé, sur lequel doit être basée la version

Une version est basée sur un numéro de commit. C'est la règle. Il est donc nécessaire de repérer le numéro de commit sur lequel se base la version que l'on veut créer. Plutôt que de se référer au long numéro de commit, il est plus facile de se référer à son numéro abbrégé, repérable avec la commande `git log --abbrev-commit`

```
# Se placer sur la branche master
$ git checkout master

# Lister les numéros de commits abbrégés
$ git log --abbrev-commit

# Repérer le numéro de commit sur lequel doit être fondé la version (le noter pour la suite)
```

##### c) Créer les 2 branches nécessaires au versionnage du projet: la branche éphémère de versionnage + la branche éternelle du projet  (si déjà créées, passer cette étape)

Rappel de la convention pour nommer les 2 branches: 

  * Convention nom d'un projet: **PROJET/Nom_Projet**
  * Convention nom de la branche versionnage: **VERSIONNAGE/Nom_Projet-NumVersion**

```
# Se placer sur la branche master en local
$ git checkout master

# Créer la branche éternelle du projet en local
$ git branch PROJET/Nom_Projet

# Créer la branche éphémère de versionnage en local sur la base du  numéro de commit choisi
$ git branch VERSIONNAGE/Nom_Projet-NumVersion NumCommit

# Pousser la création des branches locales sur le dépôt distant
$ git push -u origin PROJET/Nom_Projet
$ git push -u origin VERSIONNAGE/Nom_Projet-NumVersion
```

##### d) Rembobiner l'historique pour être à jour

```
# Se placer sur la branche du projet
$ git checkout PROJET/Nom_Projet

# Importer les informations du dépôt distant
$ git fetch origin

# Rembobiner l'historique de la branche master
$ git rebase origin/master

# Rembobiner l'historique de la branche éternelle du projet
$ git rebase origin/PROJET/Nom_Projet

# Rembobiner l'historique de la branche éphémère de versionnage
$ git rebase origin/VERSIONNAGE/Nom_Projet-NumVersion
```

##### e) Se placer sur la branche de versionnage, puis sélectionner les fichiers de versionnage

```
# Se placer sur la branche éphémère de versionnage du projet
$ git checkout VERSIONNAGE/Nom_Projet-NumVersion
```

Puis,sélectionner, conserver, les fichiers qui concernent uniquement le projet en question, et pas les autres fichiers, pour en faire une version dédiée exclusivement à ce projet-ci, et pas aux autres projets du dépôt.

Exemple: 
```
Versionnage du projet_B
Sélection de fichiers
========================

Fichiers avant sélection
------------------------
Racine
  |___Projet_A
  | |_ Fichier1
  | |_ Fichier2
  |
  |__Projet_B
    |_ Fichier3
    |_ Fichier4

Fichiers après sélection
------------------------
Racine
  |__Projet_B
    |_ Fichier3
    |_ Fichier4

Seuls les fichiers du projet B et son répertoire sont maintenus, tous les autres fichiers et répertoires sont supprimés
```

##### f) enregistrer la sélection des fichiers sur la branche éphémère de versionnage en local et sur le dépôt distant

```
# Se placer sur la branche éphémère de versionnage du projet
$ git checkout VERSIONNAGE/Nom_Projet-NumVersion

# Enregistrer la sélection de fichiers et de répertoires conservés pour le versionnage, en local
$ git add -A .
$ git commit   # commenter dans l'éditeur qui s'ouvre

# Pousser la sélection de fichiers sur le dépôt distant
$ git push -u origin VERSIONNAGE/Nom_Projet-NumVersion
```

##### g) Créer l'étiquette de versionnage

```
# Rester sur la branche éphémère de versionnage du projet
$ git checkout VERSIONNAGE/Nom_Projet-NumVersion

# Définir une étiquette de versionnage
# Attention !!!!
#  Le nom du tag ne doit pas être le nom de la branche. 
#  Le nom de la branche commence par VERSIONNAGE/
#  Le numéro de l'étiquette ne commence pas par VERSIONNAGE/
#  Si vous faites cette erreur, il vous faudra supprimer le tag
#  localement et sur le dépôt distant, puis recommencer.

# Définir une étiquette de versionnage contenant la version
$ git tag Nom_Projet-NumVersion  
```

##### h) Versionner sur la branche éternelle du projet

```
# Se placer sur la branche éternelle du projet spécifique
$ git checkout PROJET/Nom_Projet

# Fusionner la branche éphémère de versionnage, dans la branche éternelle du projet spécifique, en local, ce qui signifie: versionner en local
$ git merge VERSIONNAGE/Nom_Projet-NumVersion

# Pousser la version effectuée localement, vers la branche du projet spécifique sur le dépôt distant, via l'étiquettage de version contenant la version
$ git push --tags origin PROJET/Nom_Projet

# Supprimer la branche éphémère de versionnage localement
$ git branch -d VERSIONNAGE/PROJET-N-NumVersion

# Supprimer la branche éphémère de versionnage sur le dépôt distant
$ git push origin --delete VERSIONNAGE/PROJET-N-NumVersion

Nota: en cas de message d'erreur lors de la suppression sur 
le dépôt distant, c'est sans doute que le nom de l'étiquettage
est identique au nom de la branche. Il faut supprimer le tag
et recommencer.
```

### Corriger des erreurs rapidement

Il s'agit ici d'apporter un correctif rapide. Pas de faire de gros travaux. De gros travaux entrent dans la logique de création de branches éphémères "PROPOSITION/".

Dans les instructions ci-dessous, les termes `id_correctif` et `ref_a_corriger` signifient:

*  **si c'est une version qui doit être corrigée**: l'identifiant id_correctif est le numéro de version qui suit le numéro de version à corriger. Dans ce cas ref_a_corriger est le numéro de version à corriger.
*  **dans tous les autres cas**: l'identifiant id_correctif est laissé à l'appréciation de chacun et ref_a_corriger est le numéro de commit sur lequel se fonde le correctif apporté. En l'absence de ce numéro ref_a_corriger, la branche sera créée automatiquement sur le dernier commit existant.



#### cas n°1: correctif à apporter hors version 

Rappel : Il s'agit ici d'apporter un correctif rapide. Pas de faire de gros travaux. De gros travaux entrent dans la logique de création de branches éphémères "PROPOSITION/".


```
# Se placer sur la branche master
$ git checkout master

# Se mettre à jour du dépôt distant
$ git pull origin master

# Créer et se placer sur une branche du correctif en local
$ git checkout -b CORRECTIF/id_correctif ref_a_corriger

```

Réaliser les correctifs dans le répertoire local.

Puis:

```
# Rester dans la branche du correctif
$ git checkout CORRECTIF/id_correctif

# Enregistrer les correctifs en local
$ git add -A .

# Publier les changements en local
$ git commit
# commenter dans l'éditeur: 1 ligne = Titre; puis retour chariot 2 fois puis lignes de détails

# Pousser les changements vers le dépôt distant
$ git push -u origin CORRECTIF/id_correctif

# Se placer sur la branche master en local
$ git checkout master

# Obtenir les éventuels changements effectués entre temps sur la branche master du dépôt distant
$ git pull origin master

# Fusionner la branche du correctif dans la branche master, en local
$ git merge CORRECTIF/id_correctif

# Pousser la fusion vers le dépôt distant
$ git push origin master

# Détruire la branche du correctif en local
$ git branch -d CORRECTIF/id_correctif

# Détruire la branche du correctif sur le dépôt distant
$ git push origin --delete CORRECTIF/id_correctif
```

#### cas n°2: Apporter un correctif à une version présente sur la branche master

Rappel: Il s'agit ici d'apporter un correctif rapide. Pas de faire de gros travaux. De gros travaux entrent dans la logique de création de branches éphémères "PROPOSITION/".

```
# Se placer sur la branche master
$ git checkout master

# Se mettre à jour du dépôt distant
$ git pull origin master

# Créer une branche du correctif en local
# Attention !
#   id_correctif est le num de version qui suit 
#   la version à corriger
#   ref_a_corriger est le numéro de version
#   à corriger
#   Exemple: git branch CORRECTIF/0.1.a.2 0.1.a.1
$ git branch CORRECTIF/id_correctif ref_a_corriger

# Se placer sur la branche du correctif
$ git checkout CORRECTIF/id_correctif
```

Effectuer les correctifs dans le répertoire: modifications, ajouts, supression, de fichiers ou de répertoires.

Puis:

```
# Se positionner dans la branche du correctif
$ git checkout CORRECTIF/id_correctif

# Enregistrer les changements en local
$ git add -A .

# Publier les changements en local
$ git commit
# commenter dans l'éditeur: 1 ligne = Titre; puis retour chariot 2 fois puis lignes de détails

# Créer une étiquette contenant les correctifs en local
$ git tag id_correctif

# Se placer sur la branche master en local
$ git checkout master

# Fusionner la branche du correctif dans la branche master, en local
$ git merge CORRECTIF/id_correctif

# Pousser la fusion vers le dépôt distant
$ git push --tags origin master

# Détruire la branche du correctif en local
$ git branch -d CORRECTIF/id_correctif

# Détruire la branche du correctif sur le dépôt distant
$ git push origin --delete CORRECTIF/id_correctif
```

#### cas n°3: Apporter un correctif à une version présente sur une branche projet

Rappel: Il s'agit ici d'apporter un correctif rapide. Pas de faire de gros travaux. De gros travaux entrent dans la logique de création de branches éphémères "PROPOSITION/".

```
# Se placer sur la branche master
$ git checkout master

# Se mettre à jour du dépôt distant
$ git pull origin master

# Créer une branche du correctif en local
# Attention !
#   id_correctif est le num de version qui suit 
#   la version à corriger
#   ref_a_corriger est le numéro de version
#   à corriger
#   Exemple: git branch CORRECTIF/0.1.a.2 0.1.a.1
$ git branch CORRECTIF/id_correctif ref_a_corriger

# Se placer sur la branche du correctif
$ git checkout CORRECTIF/id_correctif
```

Effectuer les correctifs dans le répertoire: modifications, ajouts, supression, de fichiers ou de répertoires.

Puis:

```
# Se positionner dans la branche du correctif
$ git checkout CORRECTIF/id_correctif

# Enregistrer les changements en local
$ git add -A .

# Publier les changements en local
$ git commit
# commenter dans l'éditeur: 1 ligne = Titre; puis retour chariot 2 fois puis lignes de détails

# Créer une étiquette contenant les correctifs en local
$ git tag id_correctif

# Se placer sur la branche du projet versionné en local
$ git checkout PROJET/Nom_Projet

# Fusionner la branche du correctif dans la branche master, en local
$ git merge CORRECTIF/id_correctif

# Pousser la fusion vers le dépôt distant
$ git push --tags origin PROJET/Nom_Projet

# Détruire la branche du correctif en local
$ git branch -d CORRECTIF/id_correctif

# Détruire la branche du correctif sur le dépôt distant
$ git push origin --delete CORRECTIF/id_correctif

# Revenir sur master
$ git checkout master
```


Bonne chance
