(WIP): Work In Progress
(TEC): Travail En Cours


# GUIDE DE CONTRIBUTION SUR CETTE INSTANCE FRAMAGIT/GITLAB POUR CE PROJET MATÉRIEL LIBRE

Ce guide est en 3 parties.
*   1ère partie: principe général de contribution
*   2ème partie: guide pour les personnes utilisatrices de l'interface web Framagit/gitlab
*   3ème partie: guide pour les personnes qui utilisent GIT en ligne de commande

Lorsque vous souhaitez proposer un élément nouveau, ou la modification d'un élément, les guides ci-dessous peuvent vous aider.

**Prérequis:**
*   Avoir déjà utilisé gitlab, ou avoir déjà utilisé git en ligne de commande
*   Savoir créer un clone depuis une instance projet framagit, vers une machine locale
*   Avoir compris la notion de branches

## I. Principe général de contribution

*   On n'écrit jamais directement dans la branche `master`
*   La branche `master` est la branche par défaut, elle est protégée.
*   Chaque envie d'ajout, de modification, se fait sur des branches créées à partir de `master`
*   Le nom des branches créées, devraient toutes commencer par le mot "PROPOSITION" en majuscule, suivi d'un numéro _année_moi_jour_ , suivi d'un code à 4 chiffres au hasard, suivi puis d'une phrase explicite 
    *  (exemple: PROPOSITION-18-08-04-B453-amelioration-du-fichier-contributing.md)
*   Dès que nos travaux sont à un stade intermédiaire suffisament présentable, on pousse les travaux sur le site distant framagit, de telle façon à favoriser les échanges sur les progressions, de telle façon à faire connaître aux autres ce que l'on fait.
*   On cherche régulièrement à se mettre à jour de ce que font les autres.


## II. Guide pour les personnes utilisatrices de l'interface web distante framagit/gitlab

Si vous êtes une personne qui utilise GIT exclusivement en lignes de commande, il est préférable que vous lisiez la 3ème partie de ce guide.

Lorsque vous souhaitez contribuer à cette instance materiel-libre, en utilisant l'interface web framagit/gitlab, voici, ci-dessous, des principes contributifs que vous pouvez préférer suivre.

### Principes de contribution proposés (résumés) / utilisation de l'interface framagit/gitlab
1.  On écrit jamais dans la branche `master` directement
2.  Tout(e) ajout/modification/contribution/proposition, commence par la création d'une issue, que l'on cherche à discuter avec les membres de ce projet avant de faire quoique ce soit d'autre
3.  Chaque issue est ensuite transformée en branche en choississant "create a branch" sur `master` depuis la page de l'issue, en donnant à la branche, le nom de l'issue (nom d'issue = nom de branche)
4.  Lorsque la contribution sur la branche est relativement avancée, à un stade intermédiaire, ne pas hésiter à proposer une `merge request` en plaçant en début de titre de la `merge request` les trois lettres WIP suivi d'un ":", comme ceci "WIP: demande de merge request", en assignant personne, et en proposant de conserver la source.
5.  Cette `merge request` à un stade intermédiaire, permet de discuter, d'avoir des retours, des avis, des suggestions, d'effectuer les évolutions éventuelles, ou de progresser plus sereinement dans des directions
6.  Une fois les modifications finalisées, discutées, refaire une `merge request`
7.  La fusion se fera alors


## III. Guide pour les personnes expérimentées utilisant GIT en ligne de commandes

Inspiré du [Doc Workflow git simplifié de Altlasian en ligne de commandes](https://www.atlassian.com/blog/archives/simple-git-workflow-simple).

Vous préférez utiliser git en ligne de commande ? Voici comment vous pouvez contribuer.

### 0. PRÉAMBULE: CLONEZ LE DÉPÔT DISTANT
    
    git clone https://framagit.org/materiel-libre/materiel-libre.git
    
ou bien: 
    
    git clone git@framagit.org:materiel-libre/materiel-libre.git
    
Une fois cloné, effectuer les étapes suivantes à chaque fois que vous voulez contribuer en ligne de commande, de façon participative, entre votre dépôt local et le dépôt distant framagit de ce projet matériel libre.
    
### 1. Récupérer les derniers changements effectués sur le dépôt distant
    
    git checkout master
    git fetch origin
    git merge master
    
### 2. Placez-vous sur une branche dédiée

Recherchez dans les branches du dépôt distant celle sur laquelle vous voulez contribuer.
    
    git branch -a
    git checkout PROPOSITION-180704-V5R4-amelioration-de-quelque-chose
    
Ou bien créez une branche explicite:

    git checkout -b nom-de-branche-explicite
    
Essayez de donner un nom explicite à votre branche, en commençant par le mot `PROPOSITION` puis par la date `aammjj`, puis par un code de 4 caractères composés de lettres ou de chiffres, et enfin par une expression explicite de votre proposition, séparés par des tirets du 6 ( - ), ce qui donne la forme suivante:

    PROPOSITION-aammjj-code-action-explicite

Exemple:
 
    PROPOSITION-180704-A4A6-ce-guide-est-naze-il-faudrait-l-ameliorer
    
### 3. Faites vos modifications sur votre dépôt local

Apportez vos modifications depuis cette branche aussi longtemps que vous le souhaitez. 

Rédigez des titres de commits explicites sur votre dépôt local:
    
    git add NOM_FICHIER_OU_NOM_REPERTOIRE
    git commit -m "COMMENTAIRE ET MESSAGE CONCERNANT LE COMMIT"

### 4. Maintenez à jour master et la branche de votre dépôt local, avec le dépôt distant

Prenez en compte des éventuels modifications qui pourraient être apportées par d'autres sur le dépôt distant:
    
    git fetch origin
    git rebase origin/master
    git rebase origin/PROPOSITION-180704-V5R4-amelioration-de-quelque-chose
    
À ce stade, solutionnez les éventuels conflits qui sont retournés par la commande rebase. 
Résolvez ces conflits pendant le rebase: cela vous garanti d'avoir toujours des merges propres à la fin du développement de votre contribution. Cela garde aussi un historique propre de votre contribution.
    
### 5. Partagez le plus tôt possible, le développement intermédiaire de votre contribution avec les autres, en poussant vos contrib' depuis votre dépôt local vers le dépôt distant
    
Quand vous arrivez à un stade intermédiaire suffisament compréhensible, partagez le développement de votre contribution avec les autres.
Procédez à un rebase comme expliqué au point 3, puis poussez votre contribution sur le dépôt distant:
    
    git checkout PROPOSITION-180704-V5R4-amelioration-de-quelque-chose
    git push origin PROPOSITION-180704-V5R4-amelioration-de-quelque-chose

À ce stade, continuez à améliorer les choses en répétant les étapes 2 à 4 au fur à mesure de vos améliorations, autant de fois que vous le souhaitez, autant de fois que nécessaire, autant de fois que le projet collectif le demande.
    
### 6. Terminé ? demandez que vos travaux soient fusionnés au projet sur le dépôt distant

Une fois que votre contribution est terminée, une fois que les autres personnes impliquées dans le projet collectif, ont vérifié que tout va bien, ont révisé votre contribution alors c'est l'heure d'effectuer une fusion (merge)
    
    git rebase -i origin/master
    git checkout master
    git pull origin master
    git merge --no-ff PROPOSITION-180704-V5R4-amelioration-de-quelque-chose
    
La commande rebase permet d'avoir une dernière mise à jour valide juste avant de fusionner.

L'étiquette -no-ff preserve le contexte de votre travail ce qui rend plus facile un retour vers votre contribution complète si nécessaire. La fusion est juste un marqueur contenant le contexte de votre contribution.

-


## IV. Guide pour les personnes peu expérimentées utilisant GIT en ligne de commandes

Si vous êtes une personne qui utilise l'interface web distante de framagit/gitlab plutôt que les lignes de commande depuis votre machine locale il est préférable que vous lisiez la 1ère partie de ce guide.

Lorsque vous souhaitez contribuer à cette instance Matériel Libre, en utilisant exclusivement les lignes de commande git depuis votre machine locale, sans  utiliser l'interface web framagit/gitlab, voici, ci-dessous, des principes contributifs que vous pouvez préférer suivre.

### Principes de contribution proposés (résumés) / ligne de commande sur machine locale, non-utilisation de l'interface framagit/gitlab

Préliminaire: on part du principe que vous avez l'habitude de vous servir de git en ligne de commande sur votre machine locale. Si ce n'est pas le cas, vous devriez plutôt vous créer un compte sur framagit et contribuer au projet via l'interface web de framagit. Ce sera plus facile pour vous.

Les principes exprimés ci-dessous, sont inspirés des documentations d'utilisation en ligne de commande: 
*   [Doc gitlab en ligne de commandes](https://docs.gitlab.com/ee/gitlab-basics/command-line-commands.html);
*   [Doc gitlab pour commencer avec les commandes git](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html);
*   [Doc Workflow git simplifié de Altlasian en ligne de commandes](https://www.atlassian.com/blog/archives/simple-git-workflow-simple).

Si vous recherchez de la documentation sur les commandes git pour un travail collaboratif: 
*   Les principales commandes GIT avec [l'excellent guide simplissime "no deep shit" pour git](https://rogerdudler.github.io/git-guide/) qui permet de capter les essentiels de git en quelques lignes limpides.
*   Le guide très simple à lire et très pédagogique [Les bases pour travailler avec les dépôts distants](https://git-scm.com/book/fr/v1/Les-bases-de-Git-Travailler-avec-des-d%C3%A9p%C3%B4ts-distants)
*   Le guide très simple à lire et très pédagogique pour [le travail collaboratif avec git](https://git-scm.com/book/fr/v2/Git-distribu%C3%A9-Contribution-%C3%A0-un-projet)

#### 1. Avant toute chose, cloner le projet materiel-libre sur votre machine locale (si ce n'est pas encore fait)

Avec un navigateur web, rendez vous sur la page web du projet, pour connaître les URL de clonage du projet [URL du projet Matériel Libre sur Framagit](https://git.framasoft.org/materiel-libre/materiel-libre)

Sur votre machine locale, placez-vous dans un répertoire que vous utilisez pour GIT. Puis, ouvrez un terminal dans ce répertoire (sur Ubuntu, clic droit puis "ouvrir un terminal"). Puis choisissez l'une ou l'autre méthode de clonage:

    # clonage en utilisant https
    git clone https://framagit.org/materiel-libre/materiel-libre.git

    # ou clonage en utilisant ssh
    git clone git@framagit.org:materiel-libre/materiel-libre.git
    
Un clone du projet materiel-libre présent sur framagit, est créé sur votre machine locale. 

#### 2. Ensuite, commencer par récupérer les derniers changements effectués sur le site distant de framagit
Vous pouvez avoir cloné le projet il y des semaines, des mois, des années, ou il y a quelques secondes. Entre temps, il est possible que des modifications aient été réalisées. Le clone que vous avez sur votre machine locale, n'est pas forcément à jour. Il est préférable de récupérer sur votre machine locale, les éventuels changements qui ont eut lieu sur le site framagit.

Dans un terminal, placez-vous dans le répertoire materiel-libre (dans le navigateur de fichier, sur Ubuntu, clic droit "ouvrir un terminal", ou bien une commande du type `cd chemin/../materiel-libre`).

    # MÉTHODE DOUCE, PAS À PAS
    # =========================
    
    # placez vous dans la branche master
    git checkout master         # vous vous placez dans la branche master
    
    # récupérer les derniers changements sur la branche master
    # sans écraser votre travail local
    git fetch origin            # vous synchronisez le master de votre machine locale avec le master du dépôt distant

    # fusionner le site distant sur votre travail local
    git merge master            # la branche master de votre dépôt local devient l'identique du master du dépôt distant
    
    # MÉTHODE PLUS GÉNÉRALE
    # =====================
    
    # placez vous dans la branche master
    git checkout master         # vous vous placez dans la branche master

    # récupérer les derniers changements sur toutes les branches
    # sans écraser votre travail local
    git fetch --all           # vous synchronisez votre machine locale avec tout le dépôt distant

    # fusionner le site distant sur votre travail local
    git merge                 # votre dépôt local devient l'identique de tout le dépôt distant

    # MÉTHODE RADICALE
    # ================

    # placez vous dans la branche master
    git checkout master         # vous vous placez dans la branche master

    # écrasez tout votre dépôt local par toutes les données de tout le dépôt distant
    # Attention ! c'est radical !
    git pull --all              # tout votre dépôt local est totalement écrasé par tout le dépôt distant.


#### 3. Placez vous sur une branche dédiée pour pouvoir effectuer votre contribution au bon endroit
Vous souhaitez apporter votre contribution au projet. Pour cela, il vous faut travailler sur une branche dédiée à la contribution que vous apportez. Car vous ne pouvez pas travailler directement dans la branche master (voir les principes généraux de contribution en section I.).

Il y deux configurations possibles. L'une et l'autre sont compatibles.
*   vous souhaitez travailler sur une branche déjà créée sur le dépôt distant, dédiée au sujet auquel vous souhaitez contribuer (travail collaboratif)
*   ou bien, vous souhaitez créer une branche spéciale pour votre contribution, qu'une branche dédiée ait déjà été créée ou pas sur le dépôt distant (travail plus individuel)

Détecter la liste des branches présentes sur le dépôt distant et dans le dépôt local
    git branch -a
    
Comparez avec la liste des branches uniquement présentes sur le dépôt distant
    git branch -r

Toute branche qui apparaît dans le retour de `branch -a` et qui n'est pas dans la liste du retour de commande `branch -r`, est une branche qui est présente sur votre dépôt local, et qui n'est pas présente sur le dépôt distant.

Repérez dans cette liste, la branche sur laquelle vous voulez travailler.
Dans cet exemple, le nom de la branche est PROPOSITION_18_08_04_5A34_AC_MODIFICATION_EN_COURS.
La structure du nom de la branche choisie ici et faite un copie de son nom:

    ctrl_C de PROPOSITION_433_MODIFICATION_EN_COURS

Si la branche espérée n'est pas présente, ou si vous souhaitez créer cette nouvelle branche, ce qui suit convient aussi.

Puis, placez-vous dans cette branche. 

    # placez-vous dans la branche PROPOSITION_433_MODIFICATION_EN_COURS
    git checkout PROPOSITION_433_MODIFICATION_EN_COURS 
    
    # si vous avez un message qui vous retourne que la branche n'existe pas, alors il faut la créer:
    git branch PROPOSITION_433_MODIFICATION_EN_COURS
    # puis placez-vous dans cette branche créée:
    git checkout PROPOSITION_433_MODIFICATION_EN_COURS
    
    


----
TOUT CE QUI SUIT EST EN VERSION BROUILLON (À RÉPARER ! FIXME !)

1.  Mettez à jour votre machine locale en recherchant les modifications qui ont été réalisées sur le site distant framagit du projet materiel-libre (se référer aux commandes `git fetch` et `git pull`).
    *   vous pouvez faire un `git fetch --all` ce qui préserve vos travaux sur votre machine locale, tout en vous donnant des indications sur ce qui a été fait sur le site distant framagit
    *   ou vous pouvez faire un `git pull --all` ce qui va écraser vos travaux réalisés localement, ce qu'il va y avoir dans votre dossier materiel-libre sur votre machine locale, sera ce qu'il y a sur le site distant.
2.  Depuis le terminal, dans le répertoire local materiel-libre, repérez les branches existantes du projet
    *   `git branch`
3.  Depuis le terminal, dans le répertoire local materiel-libre, repérez dans quelle branche vous êtes:
    *   `git status`
4.  Depuis le terminal, dans le répertoire local materiel-libre, placez-vous dans la branche sur laquelle vous voulez contribuer
    *   `git checkout la-branche-souhaitee`
5.  À partir de cet instant, vous devez avoir en visuel dans votre navigateur de fichiers, les fichiers et les répertoires de la branche en question. Vous pouvez alors travailler sur ces fichiers et répertoires, et ensuite, adresser vos modification au site distant via les commandes git classiques (commit, merge, etc ...). Exemple de commandes pour pousser vos modifications vers la branche:
    *   `git add NOM_FICHIER_OU_NOM_REPERTOIRE`
    *   `git commit -m "COMMENTAIRE ET MESSAGE CONCERNANT LE COMMIT"`
    *   `git push origin NOM_DE_LA_BRANCHE`

**Sources:**
*   source : (fixme)
