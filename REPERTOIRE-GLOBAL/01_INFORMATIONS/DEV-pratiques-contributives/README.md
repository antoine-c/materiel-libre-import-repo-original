## LISEZ-MOI

Ce fichier donne des informations sur le répertoire courant.

### Contenu de ce répertoire

Ce répertoire est créé pour recevoir des fichiers liés à la rédaction d'un texte de principes de contribution.

Les fichiers sont classés avec un ordre chronologique de lecture: le fichier 00 est à lire en premier; le ficier 99 est à lire en dernier.

