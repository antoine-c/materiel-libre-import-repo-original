# PLANIFICATION

## 01. Récolter les informations sur les workflows recommandés
*   Voir: [récolte info sur les workflows recommandés](https://framagit.org/materiel-libre/materiel-libre/blob/14-ecrire-le-fichier-guide-de-contribution-pour-le-workflow-de-ce-projet/pratiques-contributives/recolte-informations-sur-les-workflow-git.md) 
    *   Progression: [==========>] [100 %]

## 02. Analyser les informations récoltées sur les workflows recommandés
*  Voir: [analyse des infos sur les workflow recommandés](https://framagit.org/materiel-libre/materiel-libre/blob/14-ecrire-le-fichier-guide-de-contribution-pour-le-workflow-de-ce-projet/pratiques-contributives/analyse-info-recoltees-sur-les-wrokflow-git.md) 
    *   Progression: [==========>] [100 %]

## 03. Planification
Objet de ce présent document
Progression: [=======>---] [70 %]

## 04. Actions

### 04.01. Déplacer les branches issues depuis la branche développement vers la branche master

#### 04.01.01. Faire un test de déplacement sur une instance projet test
1.   Créer et alimenter un fichier pour documenter la procédure réalisée, et les retours de cette procédure 
     *   Progression: [==========>] [100 %]
2.   Créer une instance projet de test reproduisant l'instance actuelle (master, develop, branche par défaut, branche issue) [==========>] [100 %]
3.   Sur cette instance de test, tester l'action `merge` la branche `developement` dans la branche `master` en conservant l'historique de la branche `developpement`. Test à réaliser sur une instance de test pour vérifier que les branches qui sont pointées sur la branche développemnt, seront pointées ensuite sur la branche master, et que ces branches ne vont pas disparaître. [==========>] [100 %]

#### 04.01.02. Déplacer les branches 
1.   Créer un fichier pour documenter la procédure de déplacement [>----------] [0 %]
2.   Sur l'instance projet materiel libre: `merge` la branche `developement` dans la branche `master` en conservant l'historique de la branche `developpement`. [==========>] [100 %]

#### 04.01.03. Faire du ménage dans les branches déplacées 
1.   Examiner et faire du ménage dans les branches, les fichiers, les répertoires (les éléments en projet) [>----------] [0 %]

### 04.02. Rédiger le document décrivant le work flow
1.  créer le document, et le rédiger en pensant à différencier les utilisations distantes / locales [>----------] [1 %]
2.  demander une fusion dans master, une fois le document présentable à la discussion [>----------] [0 %]