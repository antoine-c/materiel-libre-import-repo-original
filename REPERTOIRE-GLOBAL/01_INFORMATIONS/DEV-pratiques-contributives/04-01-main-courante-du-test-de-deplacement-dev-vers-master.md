## MAIN COURANTE DU TEST DE DÉPLACEMENT DE LA BRANCHE DEV VERS MASTER SUR UNE INSTANCE DE TEST

### Reproduction de la configuration actuelle (juillet 2018)
1.  Création d'une instance de test privée sur framagit (nouveau projet) appelé `instance-de-tests`
2.  Création d'une branche `dev` sur la branche `master`
3.  Protection de la branche `master`
4.  Définition de La branche `dev` comme branche par défaut
5.  Création d'un fichier texte de test (contenu sans intérêt) sur la branche `dev`
6.  Création d'une issue de test (contenu sans intérêt)
7.  Création d'une branche `issue` depuis cette issue, sur la branche `dev`
8.  Création d'un fichier texte de test (contenu sans intérêt) sur la branche `issue`


### Tentative Réussie de fusion de dev dans master

1.  Définition de la branche `master` comme branche par défaut (de telle façon à permettre de faire une demande fusion, sinon, ce n'est pas possible)
2.  Sélection de la branche `dev` et réalisation d'une demande de `merge` de cette branche `dev` vers la branche `master`
   * en plaçant les trois lettres WIP au début du titre de la `merge request`
   * `Assignee`, `Milestone` et `lable` non renseignés
   * `Remove source branch when merge request is accepted.` décoché
   * `Squash commits when merge request is accepted` décoché
3.  Merge après avoir retiré le WIP (solve wip), en cliquant sur le bouton vert `merge` (`remove source branch` décoché)
4.  RÉSULTAT POSITIF: la branche `issue` se trouve branchée sur la branche `master`; La branche `dev` est toujours là, comme une sorte d'archive si on veut y revenir. Elle pourra être supprimée une fois devenue inutile.
   
