## TRAVAUX EN COURS

### liens directs vers du contenu clef

*  lien direct vers ce que nous faisons actuellement:
   *  lien direct vers [l'invitation à co-construire une licence de matériel libre](https://framagit.org/materiel-libre/materiel-libre/issues/3)
   *  lien direct vers [discussions à propos des 10 caractéristiques d'un matériel libre idéal](https://framagit.org/materiel-libre/materiel-libre/commit/dafbe882bebc2a7a92fd5f3d65fbe48e6994564a)
   *  lien direct vers [le texte intégral des 10 caractéristiques (en construction)](https://framagit.org/materiel-libre/materiel-libre/blob/3-on-devrait-essayer-de-rediger-un-premier-texte-ultra-simple-articulant-licences-libres-existantes-et-fabrications/licence-materiel-libre/licence-incubation/mix-FLOS-licences-et-OHL/proto-fablib-lal/caracteristiques-materiel-libre-txt-complet.txt)


## INFORMATIONS SUR LES TRAVAUX EN COURS
### Rédaction d'un récapitulatif simple
Proposition d'un premier texte simple récapitulant les principes d'un matériel libre idéal. 
Nous travaillons actuellement sur la branche [3-on-devrait-essayer-de-rediger-un-premier-texte-ultra-simple-articulant-licences-libres-existantes-et-fabrications](https://framagit.org/materiel-libre/materiel-libre/tree/3-on-devrait-essayer-de-rediger-un-premier-texte-ultra-simple-articulant-licences-libres-existantes-et-fabrications)
Les documents sont entreposés [ici](licence-materiel-libre/licence-incubation/mix-FLOS-licences-et-OHL/proto-fablib-lal).
