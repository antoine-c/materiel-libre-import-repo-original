README-PRINCIPAL - Version de développement

**Avertissement:** document en cours de finalisation. Des liens peuvent ne pas fonctionner. Ou ne pas être présents. Merci pour votre compréhension.

Finalisation: [========>--] (80%)
***

INSTANCE GITLAB/FRAMAGIT D’UN PROJET GLOBAL ET EXPÉRIMENTAL RELATIF À LA NOTION DE "MATERIEL-LIBRE"
===================================================================================================

Cette instance Framagit est l’un des outils numériques utilisés pour
concevoir, produire et publier sur Internet, une partie des contenus
d’un projet global et expérimental relatif à la notion de matériel
libre, développé dans un esprit de la [recherche
appliquée](https://fr.wikipedia.org/wiki/Recherche_appliquée).

Liens directs vers des contenus « phares » :

**| Travaux en cours |**

**| Wiki | Doc | Versions |**

BIENVENUE !
-----------

Ce projet est « ouvert ». Il est « ouvert » à toutes et tous. Ses
publications sont « ouvertes » à la Connaissance – accessibles et
gratuites. Toute personne est la bienvenue, quelques-soient ses bagages
ou son niveau de compétences. Il n'est pas nécessaire de savoir coder,
ni d'avoir un niveau expert, ni d'avoir un diplôme, pour contribuer. Il
suffit juste d’en avoir envie.

DESCRIPTION
-----------

### Quels types de travaux sont réalisés sur cette instance materiel-libre ?

Les travaux réalisés via cette instance sont expérimentaux, et
consistent essentiellement :

-   à explorer, expérimenter, rechercher, développer, les différents
    aspects qui font (ou qui pourraient faire) la notion de Matériel
    Libre ;
-   à développer des textes permettant de placer des objets et leurs
    fabrications sous des termes et conditions "Free Libre Open Source"
    (FL-OS)
-   à mettre en application cette notion via des réalisations concrètes
    de fabrications d'objets développés au travers de ces travaux de
    Matériel Libre ;
-   à porter des réflexions sur la façon dont les activités économiques
    de fabrications de Matériel Libre peuvent trouver leurs places et
    devenir pérennes.

### Ce qui se fait sur cette instance « materiel-libre »

Sur cette instance Framagit, nous explorons, à distance, les champs liés
à la notion de matériel libre : propositions de sujets à traiter ;
discussions  ; publications de contenus expérimentaux (textes, dessins,
croquis, schémas, fichiers CAO, etc …) ; publications de plans de
matériels libres ; etc ...

### Ce que fait cette instance « matériel-libre », ce qu’elle délivre

Dans un esprit de recherche appliquée, ce projet publie :

-   des réflexions sur la notion de « Matériel Libre ».
-   de la documentation de recherches et développements relative à la
    fabrication de matériel libre spécifique
-   des textes expérimentaux de termes et conditions permettant de
    fabriquer des matériels en utilisant des licences FL-OS\[1\]
    applicables à la fabrication d'objets

### Pourquoi cette instance « matériel-libre » peut avoir une utilité

Ces travaux expérimentaux participent à :

-   pousser les réflexions concernant la notion de matériel libre, et à
    faire avancer cette question
-   délivrer des exemples opérationnels de fabrication de matériels
    libres

### But de cette instance « »materiel-libre »

-   publier des contenus qui peuvent avoir une utilité pour le
    développement de la notion de matériel libre

### Motivations initiales ayant conduit à la création de cette instance « matériel-libre »

Au départ, il y a la volonté d’expérimenter, à des fins expérimentales,
le portage des pratiques FL-OS (Free Libre Open Source) issues des
logiciels, dans l’univers de la fabrication et de l’exploitation à des
fins industrielles et commerciales, de choses faites avec des atomes
(Things Made With Atoms Fabrications – « TMAs Fabrications »).

Très vite, il est apparu nécessaire de pouvoir utiliser un outil
numérique à distance qui permette d’échanger, de discuter, de publier,
de modifier, de façon participative, des contenus relatifs à la notion
de matériel libre, de différents formats (textes, codes, croquis,
dessins, schémas, plan, fichiers CAO, etc ….) tout en gérant le
versionnage partagé de ces contenus.

Git est très utilisé pour le développement des logiciels FL-OS (Free
Libre Open Source Software - FL-OS-S). Dans l’idée d’expérimenter les
pratiques issues des FL-OS-S, il a donc semblé intéressant d’essayer de
l’utiliser pour cette idée de projet expérimental relatif à la notion de
matériel libre.

Cette instance framagit « matériel-libre » est l’un des outils utilisé
par le projet global et expérimental relatif à la notion de matériel
libre.

### Intentions de départ dans l’utilisation qui est faite de cette instance « matériel-libre »

Dans l’idée d’expérimenter le portage des pratiques FL-OS-S dans
l’univers de la fabrication de choses matérielles (notion de matériel
libre), l’intention de départ consiste à utiliser framagit comme
pourrait le faire un projet logiciel FL-OS-S. Notamment :

-   mettre en ligne de façon ouverte et partagée, les réflexions du
    projet et ses réalisations sur la notion de matériel libre, afin
    d'inviter les personnes à participer à ces travaux.
-   explorer la notion de matériel libre sous ses différentes facettes
    (y compris son utilisation à des fins industrielles et commerciales)
    en délivrant des contenus utilisables par le plus grand nombre
    permettant à chacune et à chacun d'alimenter sa propre opinion sur
    la notion de matériel libre et sur sa mise en œuvre
-   délivrer les processus R&D, leurs éléments et contenus, sous
    licences FL-OS-L (Free Libre Open Source Licences) appropriées
-   gérer le versionnage des contenus pour aboutir à des versions

STATUT DU DÉVELOPPEMENT DE CETTE INSTANCE « MATERIEL-LIBRE »\
-------------------------------------------------------------

Statut « **pre-alpha » :**

-   pas encore testé
-   pas encore vérifié
-   instable
-   pas encore analysé totalement
-   peut-être : erroné, faux, non-fiable
-   non-opérationnel

SAVOIR-FAIRE REQUIS POUR PARTICIPER À CETTE INSTANCE « MATÉRIEL-LIBRE »
-----------------------------------------------------------------------

Il n'est pas nécessaire d'être diplômé pour participer à ce projet. Tout
le monde est invité à poser des questions, à suggérer des idées, etc ...

Il n'est pas nécessaire d'avoir obligatoirement une compétence
"technique", ou de savoir "coder". Un texte, un dessin, un croquis, même
très mal fait, cela nous va aussi très bien.

PAR OÙ COMMENCER POUR CONTRIBUER À CETTE INSTANCE « MATÉRIEL-LIBRE »
--------------------------------------------------------------------

C’est expliqué ici

OÙ TROUVER DE L'AIDE POUR CONTRIBUER À CETTE INSTANCE « MATÉRIEL-LIBRE »
------------------------------------------------------------------------

-   Support
-   Contact

QUI MAINTIENT ET CONTRIBUE À CETTE INSTANCE « MATÉRIEL-LIBRE »
--------------------------------------------------------------

-   [groupe materiel-libre](https://framagit.org/materiel-libre)

VISUELS DU MOMENT RELATIFS À CETTE INSTANCE « MATÉRIEL-LIBRE »
--------------------------------------------------------------

FIXME !

USAGES DE CETTE INSTANCE « MATÉRIEL-LIBRE »
-------------------------------------------

Vous pouvez créer une variante (un « fork) de cette instance framagit
matériel libre, en faisant un clone :

-   ssh : <git@framagit.org>:materiel-libre/materiel-libre.git
-   https : https://framagit.org/materiel-libre/materiel-libre.git

Les travaux expérimentaux publiés sur cette instance « matériel-libre »,
peuvent être notamment utilisables, à des fins d’inspiration ou de
production, dans le cadre de :

-   projets d’études concernant la notion de matériel libre
-   projets de la fabrication d’objets sous R&D « ouverte » (Free Libre
    Open Source FL-OS)

REMONTER UN PROBLÈME, UNE ERREUR CONCERNANT CETTE INSTANCE « MATÉRIEL-LIBRE »
-----------------------------------------------------------------------------

Vous avez détecté un problème, une erreur, quelque chose d’inexacte ?
Dites-le nous via la page dédiée, pour que nous corrigions ça.

INFORMATIONS COMPLÉMENTAIRES CONCERNANT CETTE INSTANCE « MATÉRIEL-LIBRE »
-------------------------------------------------------------------------

### Feuille de route concernant cette instance « matériel-libre »

Pour l’instant, la feuille de route est de faire avancer les sujets
suivants en parallèle, selon les disponibilités des personnes qui
contribuent, :

-   définition de la notion de matériel libre
-   rédaction de déclinaison de termes et conditions spécifiques à la
    fabrication de matériels libres
-   publication de documentation de conception et fabrication de
    matériel libre spécifique : une mini-éolienne ; …

### Travaux en cours sur cette instance « matériel-libre »

La liste des travaux en cours est disponible ici.

### Contribuer à cette instance « matériel libre »: qui peut contribuer; guide pour contribuer; pratiques contributives

**Qui peut contribuer :** dans la théorie, tout le monde. Pour les
personnes qui n’utilisent pas git ou framagit, il existe d’autres moyens
pour contribuer

**Guide pour contribuer :** que vous maîtrisiez ou non framagit (ou
git), une page dédiée est là pour vous aider à comprendre comment
contribuer à cette instance « matériel-libre ».

**Pratiques contributives :** contribuer à plusieurs à distance, est
agréable, surtout lorsque l’on a conscience de pratiques qui favorisent
le plaisir à participer à ce type de projet. À lire absolument !

### Connaissances et savoirs mis en jeu dans cette instance « matériel-libre » projet

La liste des savoirs techniques et des savoirs non-techniques mis en jeu
dans cette instance « matériel-libre » est relativement vaste. On en
parle ici.

### Exemple de contribution sur cette instance « matériel-libre »

-   Faire un croquis à la main sur un dessus de nappe, le prendre en
    photo et le publier sur une discussion de l’instance
    « matériel-libre » pour alimenter les échanges
-   Écrire un bout de code arduino pour faire un PID permettant de
    ralentir magnétiquement une mini-éolienne
-   Publier une interrogation concernant un terme utilisé dans la
    rédaction du texte « les 10 caractéristiques du matériel libre »

### Utilisation des contenus de cette instance « matériel-libre »

Mode d'emploi, utilisation (comment utiliser); pré-requis à
l'utilisation; limitation à l'utilisation; pourquoi utiliser

### Tests des contenus de cette instance « matériel-libre »

Les tests des trucs que l’on développe, ne sont pas encore d’actualité
(statut « pré-alpha » oblige).

Cependant, il est possible de les tester tout de même si le cœur vous en
dit ;-)

### Ressources documentaires de cette instance « matériel-libre »

-   Documentation : voir ici
-   Wiki sur framagit : pas encore dispo
-   Autres ressources : voir ici

### Versions concernant cette instance « matériel-libre »

En statut « pré-alpha », [des tentatives de
pré-versionnages](https://framagit.org/materiel-libre/materiel-libre/releases)
sont réalisées de temps en temps.

### À propos de cette instance « matériel-libre »

-   <https://framagit.org/materiel-libre/materiel-libre>
-   <https://framagit.org/materiel-libre>

### Auteurs, crédits concernant les contenus publiés sur cette instance « matériel-libre »

-   [Groupe materiel-libre sur
    Framagit](https://framagit.org/materiel-libre)
-   [Antoine C](https://framagit.org/Antoine) (« initial founder »,
    « fondateur initial »)

### Licence concernant les contenus publiés sur cette instance « matériel-libre »

Sous réserve de stipulation contraire, de façon générale, les contenus
sont mis à disposition sous licence CERN-OHL 1.2

### Remerciements

On remercie les trucs qui sont cachés derrière le big-bang de nous avoir
permis d’arriver jusque-là.

### Autres informations

« Quand y’en a plus, y’en a encore » (Roger, le 10 avril de l’an
d’avant, accoudé au bar en buvant une coudée)

### Notes

\[1\] : FL-OS pour Free Libre OpenSource
